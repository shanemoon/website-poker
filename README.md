# Website Poker #

### What is this script for? ###

* A simple script that pings a website every some period (optionally) plus some random noise time.

* It's useful for e.g. making sure that your server (heroku) doesn't go sleep.	(["App Sleeping on Heroku"])(https://blog.heroku.com/archives/2013/6/20/app_sleeping_on_heroku)

* Inspired by e.g. http://wakemydyno.com/

### How do I run it? ###

* On a server or on your personal computer, simply run `python website_poker.py`

* You can modify `ADDRESS` and `PERIOD` to ping.