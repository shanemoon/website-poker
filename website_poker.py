__author__ = "Shane Moon"

"""
	Author: Shane Moon
	Description: 
		A simple script that pings a website every some period,
		(optionally) plus some random noise time.
		
		It's useful for e.g. making sure that your server (heroku) 
		doesn't go sleep.
		(https://blog.heroku.com/archives/2013/6/20/app_sleeping_on_heroku)

		Inspired by e.g. http://wakemydyno.com/
"""

import time, random
import urllib2

ADDRESS = 'http://www.google.com'
PERIOD = 3000 # in seconds
NOISE = 300 # in seconds

while True:
	try:
		# Ping a website
		urllib2.urlopen( ADDRESS ).read()
		print 'Ping successful: %s' % time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
	except:
		# It wasn't able to ping it successfully
		print '** Ping unsuccessful: %s **' % time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

	# Sleep for every 'sleep_time'
	sleep_time = PERIOD + random.random() * NOISE - (NOISE / 2.0)
	print 'Going on sleep for the next %d seconds ...' % sleep_time
	time.sleep( sleep_time )
